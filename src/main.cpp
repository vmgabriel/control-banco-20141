/*
 * UNIVERSIDAD DISTRITAL FRANCISCO JOSÉ DE CALDAS
 * PROGRAMACIÓN BASICA
 * PROYECTO FINAL
 * Periodo 2014-1
 * Hecho por: Gabriel Vargas Monroy
 * Codigo: 20141020107
 */


//Control de inventario de una biblioteca a traves de archivos
#include <iomanip>
#include <string.h>
#include <iostream>
#include <fstream>
#include <stdio.h>

using namespace std;

int salir=0;
int cantidadcoincidencias=0;
int lista[100];

struct libro
{
    int codigo;
	char nombre[40];
	char autor[30];
	char editorial[30];
	char anopublicacion[6];
	char edicion[10];
};

void constructor()
{
    FILE *carga,*nombre,*autor;
    carga=fopen("control","a+");
    fclose(carga);
    nombre=fopen("Libros","a+");
    fclose(nombre);
    autor=fopen("Autores","a+");
    fclose(autor);
}

void limpieza()
{
    for (int i=0;i<100;i++)
    {
        lista[i]=-1;
    }
}

int lectura (char* nombre)
{
	//aqui solo se hace lectura de archivo
	FILE *archivo;
	char caracter;
	int controlador;

	archivo = fopen(nombre,"r");

	if (archivo == NULL)
	{
		cout<<"Ha ocurrido un error a la hora de llamar las dependecias, lo que quiere decir que esta mal instalado"<<endl;
		controlador=0;
    }
    else
    {
		cout<<"---"<<endl;
		cout<<"Los Libros en inventario son:"<<endl;
		cout<<"---"<<endl;
		cout<<"---------------------------------------"<<endl;
        cout<<"Id   |  Nombre | Autor | editorial | año_publicacion | edicion"<<endl;
        cout<<"---------------------------------------"<<endl;
	    while (feof(archivo) == 0)
	    {
			caracter = fgetc(archivo);
			printf("%c",caracter);
	    }
	    cout<<"................................................."<<endl;
	    controlador=1;
	}
    fclose(archivo);
    return controlador;
}

int salir_menu()
{
    int control;
    cout<<"----------------------"<<endl;
    cout<<"¿Desea salir?"<<endl;
    cout<<"1. Si"<<endl;
    cout<<"2. No"<<endl;
    cout<<"----------------------"<<endl;
    cin>>control;
    if (control ==1)
    {
        control=1;
    }
    else
    {
        control=0;
    }
    return control;
}

int menu_principal()
{
    int seleccion;
    cout<<"----------------------"<<endl;
    cout<<"Seleccione la opcion:"<<endl;
    cout<<"1. Nuevo Libro"<<endl;
    cout<<"2. Buscar Libro"<<endl;
    cout<<"3. Libros registrados"<<endl;
    cout<<"4. Cantidad de Libros registrados"<<endl;
    cout<<"5. Salir"<<endl;
    cout<<"----------------------"<<endl;
    cin>>seleccion;
    return seleccion;
}

libro registro()
{
    struct libro nuevo;
    cout<<"--------------------------------"<<endl;
    cout<<".............Nuevo Libro................"<<endl;
    cout<<"--------------------------------"<<endl;
    cout<<"Por favor ingrese el Nombre del libro: ";
    cin>>nuevo.nombre;
    cout<<"Por favor ingrese el Nombre del autor: ";
    cin>>nuevo.autor;
    cout<<"Por favor ingrese el Nombre de la editorial: ";
    cin>>nuevo.editorial;
    cout<<"Por favor ingrese el año de publicacion: ";
    cin>>nuevo.anopublicacion;
    cout<<"Por favor ingrese la edicion: ";
    cin>>nuevo.edicion;
    return nuevo;
}

int guardar(int linea,struct libro nn)
{
    FILE *carga,*nombre,*autor;
    nombre=fopen("Libros","a");
    autor=fopen("Autores","a");
    carga=fopen("control","a");
    linea=linea+1;
    nn.codigo=linea;
    int correr=fprintf(carga, "%d|%s|%s|%s|%s|%s\n",nn.codigo,nn.nombre,nn.autor,nn.editorial,nn.anopublicacion,nn.edicion);
    int ac=fprintf(nombre, "%s\n",nn.nombre);
    int ad=fprintf(autor, "%s\n",nn.autor);
    correr=ac+ad+correr;
    fclose(carga);
    fclose(autor);
    fclose(nombre);
    return correr;
}

int cantidad()
{
    int lineas=0;
    ifstream arch("control", ifstream::in);
    while(arch.good()) {if(arch.get()=='\n') {lineas++;}}
    return lineas;
}

void cargarlibrostitulo(char nombreabuscar[40])
{
    cantidadcoincidencias=0;
    limpieza();
    FILE *archivo;
    char texto[41];
	if (cantidad()==0)
	{
        cout<<"No es posible realizar la busqueda ya que no hay datos que buscar"<<endl;
	}
	else
	{
        archivo = fopen("Libros","r");
        if (archivo == NULL)
        {
            cout<<"Ha ocurrido un error a la hora de llamar las dependecias, lo que quiere decir que esta mal instalado"<<endl;
        }
        else
        {
            int tmp1=0,tmp2=0;
            while (feof(archivo)==0)
            {
                  fgets(texto,40,archivo);
                  for(int i=0;i<strlen(texto);i++)
                  {
                          if (nombreabuscar[0]==texto[i])
                          {
                               tmp1=0;
                               tmp2=i;
                               while ((nombreabuscar[tmp1]==texto[tmp2])&&(tmp2<strlen(texto))&&(tmp1!=strlen(nombreabuscar)))
                               {
                                      tmp1++;
                                      tmp2++;
                                      if (tmp1==strlen(nombreabuscar))
                                      {
                                           if (cantidadcoincidencias==100)
                                           {
                                                cout<<"Se ha sobrepasado el limite de coincidencias... solo se mostraran los primeros 100"<<endl;
                                           }
                                           else
                                           {
                                                cantidadcoincidencias++;
                                           }
                                      }
                               }
                          }
                  }

            }
        }
        fclose(archivo);
	}
}

void cargarautores(char nombreabuscar[40])
{
    cantidadcoincidencias=0;
    limpieza();
    FILE *archivo;
    char texto[41];
	if (cantidad()==0)
	{
        cout<<"No es posible realizar la busqueda ya que no hay datos que buscar"<<endl;
	}
	else
	{
        archivo = fopen("Autores","r");
        if (archivo == NULL)
        {
            cout<<"Ha ocurrido un error a la hora de llamar las dependecias, lo que quiere decir que esta mal instalado"<<endl;
        }
        else
        {
            int tmp1=0,tmp2=0;
            while (feof(archivo)==0)
            {
                  fgets(texto,40,archivo);
                  for(int i=0;i<strlen(texto);i++)
                  {
                          if (nombreabuscar[0]==texto[i])
                          {
                               tmp1=0;
                               tmp2=i;
                               while ((nombreabuscar[tmp1]==texto[tmp2])&&(tmp2<strlen(texto))&&(tmp1!=strlen(nombreabuscar)))
                               {
                                      tmp1++;
                                      tmp2++;
                                      if (tmp1==strlen(nombreabuscar))
                                      {
                                           if (cantidadcoincidencias==100)
                                           {
                                                cout<<"Se ha sobrepasado el limite de coincidencias... solo se mostraran los primeros 100"<<endl;
                                           }
                                           else
                                           {
                                                lista[cantidadcoincidencias]=i;
                                                cantidadcoincidencias++;
                                           }
                                      }
                               }
                          }
                  }

            }
        }
        fclose(archivo);
	}
}

int main()
{
    cout<<"---------------------------------------"<<endl;
	cout<<"Bienvenido, al control de biblioteca"<<endl;
	cout<<"---------------------------------------"<<endl;
	constructor();
	while (salir==0)
	{
        int retorno;
        int selecto=menu_principal();
        switch(selecto)
        {
            case (1):
            {
                struct libro code=registro();
                cout<<"Guardando"<<endl;
                int lineas=cantidad();
                int correr=guardar(lineas,code);
                if (correr<0)
                {
                    cout<<"Ha ocurrido un error a la hora de guardar"<<endl;
                }
                else
                {
                    cout<<"Guardado Correctamente"<<endl;
                }
            }
            break;
            case(2):
            {
                int sel;
                cout<<"----------------"<<endl;
                cout<<"1. Libro"<<endl;
                cout<<"2. Autor"<<endl;
                cout<<"----------------"<<endl;
                cout<<"Que desea Buscar:";cin>>sel;
                cout<<"----------------"<<endl;
                switch(sel)
                {
                    case(1):
                    {
                        char tituloab[40];
                        cout<<"Introduzca el nombre del Libro: ";
                        cin>>tituloab;
                        int cuantos=cantidad();
                        if (cuantos>0)
                        {
                            cargarlibrostitulo(tituloab);
                            if (cantidadcoincidencias>0)
                            {
                                cout<<"Se ha(n) encontrado "<<cantidadcoincidencias<<" Coincidencia(s)"<<endl;
                            }
                            else
                            {
                                cout<<"No Existe un Libro con ese nombre"<<endl;
                            }
                        }
                        else
                        {
                            cout<<"No hay Libros registrados"<<endl;
                        }
                    }
                    break;
                    case(2):
                    {
                        char tituloab[40];
                        cout<<"Introduzca el nombre del autor: ";
                        cin>>tituloab;
                        int cuantos=cantidad();
                        if (cuantos>0)
                        {
                            cargarautores(tituloab);
                            if (cantidadcoincidencias>0)
                            {
                                cout<<"Se ha(n) encontrado "<<cantidadcoincidencias<<" Coincidencia(s)"<<endl;
                            }
                            else
                            {
                                cout<<"No Existe un Autor con ese nombre"<<endl;
                            }
                        }
                        else
                        {
                            cout<<"No hay Libros registrados"<<endl;
                        }
                    }
                    break;
                    default :
                    {
                        cout<<"valor No permitido"<<endl;
                    }
                    break;
                }
            }
            break;
            case(3):
            {
                int col=lectura("control");
                if (col == 1)
                {
                    cout<<"Cargado satisfactoriamente"<<endl;
                }
                else
                {
                    cout<<"Error"<<endl;
                }
            }
            break;
            case(4):
            {
                int lineas=cantidad();
                cout<<"El valor de  los libros que se han registrado son: "<<lineas<<endl;
            }
            break;
            case (5) :
            {
                retorno=salir_menu();
                salir=retorno;
            }
            break;
            default :
            {
                cout<<"El numero seleccionado no hacen parte de las opciones"<<endl;
            }
        }
	}
	cout<<"---------------------------------------"<<endl;
	cout<<"Gracias por usar la aplicacion"<<endl;
	cout<<"---------------------------------------"<<endl;
	return 0;
}

